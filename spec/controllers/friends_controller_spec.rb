require "rails_helper"
require_relative "../support/devise"

RSpec.describe FriendsController, type: :controller do
  fixtures :users

  describe "GET /friends/new" do
    login_user

    it "friendship request should succeed" do
      user = users(:one)
      second_user = users(:two)

      expect(user.friend_status(second_user)).to eq nil
      get :new, params: {user_id: second_user.id}
      expect(user.friend_status(second_user)).to eq :friendship_request_by_other
      expect(second_user.friend_status(user)).to eq :friendship_request_by_self

      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_out user
      sign_in second_user
      get :new, params: {user_id: user.id}
      expect(user.friend_status(second_user)).to eq :friend
      expect(second_user.friend_status(user)).to eq :friend

    end
  end


end
