require "rails_helper"
require_relative "../support/devise"

RSpec.describe LedgersController, type: :controller do
  fixtures :users, :ledgers

  describe "GET /" do
    login_user

    it "should return 200:OK" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /ledgers" do
    login_user

    it "transfer of money to second user should succeed" do
      second_user = users(:two)

      post :create, params:  {ledger: {other_user_id:second_user.id, amount: 10}}

      expect(second_user.ledgers.last.balance).to eq(210)
    end

    it "transfer of money to second user should not succeed if funds are not sufficient" do
      second_user = users(:two)

      old_last_ledger_id = Ledger.last.id
      post :create, params:  {ledger: {other_user_id:second_user.id, amount: 10000}}
      expect(old_last_ledger_id).to eq(Ledger.last.id)
    end

    it "user should not be able to transfer money to self" do
      user = users(:one)

      old_last_ledger_id = Ledger.last.id
      post :create, params:  {ledger: {other_user_id: user.id, amount: 10}}
      expect(old_last_ledger_id).to eq(Ledger.last.id)
    end
  end
end
