class CreateLedgers < ActiveRecord::Migration[6.0]
  def change
    create_table :ledgers do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :other_user_id
      t.decimal :amount
      t.decimal :balance

      t.timestamps
    end
  end
end
