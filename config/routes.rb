Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_for :users

  resources :friends, path_names: {new: 'new/:user_id' }

  get 'ledgers/initial_balance'
  post 'ledgers/create_initial_balance'
  resources :ledgers, path_names: {new: 'new/:user_id' }

  get 'home/index'

  root to: "ledgers#index"
end
