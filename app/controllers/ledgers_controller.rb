class LedgersController < ApplicationController
  before_action :set_ledger, only: [:show]
  before_action :authenticate_user!

  # GET /ledgers
  # GET /ledgers.json
  def index
    if current_user.lacks_initial_balance?
      redirect_to '/ledgers/initial_balance'
    end

    @ledgers = Ledger.where(user: current_user).order(id: :desc)
    @users = User.where.not(id: current_user)
    @friends = current_user.confirmed_friends
  end

  # GET /ledgers/1
  # GET /ledgers/1.json
  def show
  end

  # GET /ledgers/new
  def new
    @ledger = Ledger.new
    @ledger.other_user_id = params[:user_id]
  end

  def initial_balance
    @ledger = Ledger.new
  end

  # POST /ledgers
  # POST /ledgers.json
  def create
    @ledger = Ledger.new(ledger_params)

    @ledger.user_id = current_user.id
    @ledger.amount = - @ledger.amount

    last_ledger_balance = Ledger.where(user_id: @ledger.user_id).last.balance
    @ledger.balance = last_ledger_balance + @ledger.amount

    if @ledger.other_user.lacks_initial_balance?
      render :new, locals: {user_id:  @ledger.other_user_id, alert: 'Other user did not complete setup!'}
      return
    end

    @ledger2 = Ledger.new
    @ledger2.amount =  - @ledger.amount
    @ledger2.other_user_id = @ledger.user_id
    @ledger2.user_id = @ledger.other_user_id

    last_ledger_balance = Ledger.where(user_id: @ledger2.user_id).last.balance
    @ledger2.balance = last_ledger_balance + @ledger2.amount

    ActiveRecord::Base.transaction do
      @ledger.save!
      @ledger2.save!

    rescue ActiveRecord::RecordInvalid => e
      render :new, locals: {user_id:  @ledger.other_user_id}
      return
    end

     redirect_to @ledger, notice: 'Transaction was successfully created.'
  end

  def create_initial_balance
    begin
    @ledger = Ledger.new(ledger_params)
    @ledger.user_id = current_user.id
    @ledger.balance = @ledger.amount
    @ledger.save!
    rescue ActiveRecord::RecordInvalid => e
      render :initial_balance, alert:  e
      return
    end

    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ledger
      @ledger = Ledger.find_by(user: current_user, id: params[:id])
      redirect_to root_path if @ledger.blank?
    end

    # Only allow a list of trusted parameters through.
    def ledger_params
      params.require(:ledger).permit(:other_user_id, :amount)
    end
end
