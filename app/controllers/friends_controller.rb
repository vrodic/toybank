class FriendsController < ApplicationController
  before_action :set_friend, only: [:show, :destroy]
  before_action :authenticate_user!

  # GET /friends
  def index
    @friends = current_user.confirmed_friends
  end


  # GET /friends/new
  # this confirms an existing friendship or creates a new request
  # if these were more complex actions I'd split them out in separate functions

  def new
    friend = Friend.where(other_user: current_user, user_id: params[:user_id]).take
    if friend.present?
      friend.confirmed_at = DateTime.now
      friend.save
      redirect_to root_path
      return
    end

    friend = Friend.new

    friend.other_user_id = params[:user_id]
    friend.user = current_user
    friend.save

    redirect_to root_path
  end



  # DELETE /friends/1
  # DELETE /friends/1.json
  def destroy
    @friend.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Friend was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_friend
      @friend = Friend.find(params[:id])
      @friend = nil unless @friend.user == current_user or @friend.other_user == current_user
    end

    # Only allow a list of trusted parameters through.
    def friend_params
      params.require(:friend).permit(:user_id, :other_user_id)
    end
end
