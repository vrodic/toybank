json.extract! friend, :id, :user_id, :other_user_id, :created_at, :updated_at
json.url friend_url(friend, format: :json)
