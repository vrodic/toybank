json.extract! ledger, :id, :user_id, :other_user_id, :amount, :balance, :created_at, :updated_at
json.url ledger_url(ledger, format: :json)
