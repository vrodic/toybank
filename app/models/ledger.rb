class Ledger < ApplicationRecord
  belongs_to :user
  belongs_to :other_user, class_name: 'User', foreign_key: 'other_user_id', optional: true

  validates :balance, numericality: {greater_than_or_equal_to: 0}
  validates :amount, numericality: {other_than: 0}
  validate :check_users_not_same

  def check_users_not_same
    errors.add(:other_user_id, "users in transaction cannot be the same") if user == other_user
  end
end
