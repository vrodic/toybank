class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :ledgers
  has_many :friends

  def lacks_initial_balance?
    self.ledgers.where(other_user_id: nil).count == 0
  end

  def friends
    Friend.where(user: self).or(Friend.where(other_user:self))
  end

  def confirmed_friends
    self.friends.where.not(confirmed_at: nil)
  end

  def friend_status(user)
    friend = Friend.where(user: self, other_user: user).take

    if friend.blank?
      friend = Friend.where(user: user, other_user: self).take
      if friend.blank?
        return nil
      end

      if friend.confirmed_at.blank?
        if friend.rejected_at.blank?
          return :friendship_request_by_self
        end
        return :friendship_rejected_by_other
      end
      return :friend
    end

    if friend.confirmed_at.blank?
      if friend.rejected_at.blank?
        return :friendship_request_by_other
      end
      return :friendship_rejected_by_self
    end

    :friend
  end
end
