class Friend < ApplicationRecord
  belongs_to :user
  belongs_to :other_user, class_name: 'User', foreign_key: 'other_user_id'

  def get_other_user(user)
    if user == self.other_user
      return self.user
    end
    self.other_user
  end

end
